
# Assignment 9 - Timers and Interrupts

## By C2C Minjong Yoon




1. How long of a delay are you using? Explain why you chose the settings you did.


The timers and interrupts that were included is actually using a 40 mili second delay. It's running through it 4 times and will total in 160 miliseconds. I used the format from Lesson 26's power point slide, but changed the 0xFFFF to 0x9C40. This iwill add up to 40 ms. 

2. How did you test your program to verify that it still worked? Be specific. Describing the various tests you used will get you off to a great start.

From the original assembly code, I had the 160 miliSecond delay subroutine. This was what I used to test my new interrupt against. When I commented out the subroutine from the assembly and had the new interrupt, the pond ran as smooth as it did when I only had the assembly subroutine. This ensure me that everything worked great. 

