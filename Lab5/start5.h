//-----------------------------------------------------------------
// Name:	Coulston
// File:	lab5.h
// Date:	Fall 2014
// Purp:	Include file for the MSP430
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// Page 76 : MSP430 Optimizing C/C++ Compiler v 4.3 User's Guide
//-----------------------------------------------------------------
typedef		unsigned char		int8;
typedef		unsigned short		int16;
typedef		unsigned long		int32;
typedef		unsigned long long	int64;

#define		TRUE				1
#define		FALSE				0

//-----------------------------------------------------------------
// Function prototypes found in lab5.c
//-----------------------------------------------------------------
void initMSP430();
__interrupt void pinChange (void);
__interrupt void timerOverflow (void);


//-----------------------------------------------------------------
// Each PxIES bit selects the interrupt edge for the corresponding I/O pin.
//	Bit = 0: The PxIFGx flag is set with a low-to-high transition
//	Bit = 1: The PxIFGx flag is set with a high-to-low transition
//-----------------------------------------------------------------

#define		IR_PIN			(P2IN & BIT6)
#define		HIGH_2_LOW		P2IES |= BIT6
#define		LOW_2_HIGH		P2IES &= ~BIT6


#define		averageLogic0Pulse	0x0311
#define		averageLogic1Pulse	0x09CE
#define		averageStartPulse	0x1100
#define		minLogic0Pulse		averageLogic0Pulse - 118
#define		maxLogic0Pulse		averageLogic0Pulse + 118
#define		minLogic1Pulse		averageLogic1Pulse - 96
#define		maxLogic1Pulse		averageLogic1Pulse + 96
#define		minStartPulse		averageStartPulse - 100
#define		maxStartPulse		averageStartPulse + 100

#define		UP		0x00542ABD
#define		DOWN	0x00544ABB
#define		LEFT	0x0055CAA3
#define		RIGHT	0x54FAB100

#define		ONE		0x57EA8100
#define		TWO		0x561A9F00
#define		THREE	0x551AAE00
#define		FOUR	0x571A8E00
#define		FIVE	0x549AB600
#define		SIX		0x569A9600
