# Lab 5 - REMOTE

## By C2C Minjong Yoon, C2C Henry Okoro




Question 1: What is the make and model of your remote?

The make and model of our remote is MAGNAVOX DFEC-19.

Question 2: What is the total length of your signal from the start bit to the stop bit?

##### Table 1: Pulse durations for captured IR signal
![entity1](table1.PNG) 


The toal length of our signal from the start bit to the stop bit is 70.298 ms.

Question 3: How long will it take the timer to roll over?

It takes about 70.298 ms to roll over. 


Question 4: How long does each timer count last?

It lasts about 1.7 ms.

Question 5: Annotate the picture below to indicate which line of the for loop in the program is executed at which part of the pulse. You should show a total of 6 lines of code (lines 32-34 and lines 36-38).  

##### Figure 1: Program Execution
![entity1](lines.PNG) 


##### Table 2: Data half-pulse charaterization
![entity1](table2.PNG) 


##### Table 3: Hexadecimal button codes
![entity1](table3.PNG) 


Question 6: Does your button transmit a different code or provide any indication that a button is being held? If so, provide a screenshot with labels and/or explanation.

My button transmit a different code everytime I hit a different button. It also indicate that a button is being held. 


##### Figure 2: Button Transmit
![entity1](yoon.PNG) 

As you can see that after the stop DATA 1, there is another set of signal that appears, which represents the button being held. 


Question 7: Given your answer to the previous question, how will you handle held button inputs in your code logic?

The fact that the code recognizes that a button is being pressed might be useful, but for the lab, I need to code it so that the program will recognize and avoid buttons being held. 